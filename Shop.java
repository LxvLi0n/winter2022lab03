import java.util.Scanner;
public class Shop{
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		Scanner scDouble = new Scanner(System.in);
		
		TShirts[] TShirts = new TShirts[4];
		
		for(int i = 0;i<TShirts.length;i++){
			
			TShirts[i] = new TShirts();
			
			System.out.println("What is the size of the T-shirt?");
			TShirts[i].size = sc.nextLine();
			
			
			System.out.println("What colour is the T-shirt?");
			TShirts[i].color = sc.nextLine();
			
			System.out.println("How much is it?");
			TShirts[i].price = scDouble.nextDouble();
			
		}
		
		System.out.println("This is the latest T-shirt you added:");
		System.out.println(TShirts[3].size);
		System.out.println(TShirts[3].color);
		System.out.println(TShirts[3].price+"$");
		System.out.println();
		
		TShirts[3].describe();
	}
}